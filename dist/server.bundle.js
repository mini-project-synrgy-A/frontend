/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("const express = __webpack_require__(/*! express */ \"express\");\nconst routes = __webpack_require__(/*! ./routes */ \"./src/routes/index.js\");\n\nconst app = express();\nconst PORT = process.env.PORT || 3030;\n\n// Static Files\napp.set('views', './src/views');\napp.set('view engine', 'ejs');\napp.use('/static', express.static('./src/public'));\napp.use('/', routes());\n\napp.use(function (err, req, res, next) {\n  if (err) {\n    console.error(err.stack);\n  }\n\n  res.status(500).send('Something broke!');\n});\n\n// Listen on Port 3000\napp.listen(PORT, () => console.info(`App listening on port ${PORT}`));\n\n\n//# sourceURL=webpack://express-validator-tut/./src/app.js?");

/***/ }),

/***/ "./src/routes/admin.router.js":
/*!************************************!*\
  !*** ./src/routes/admin.router.js ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const router = __webpack_require__(/*! express */ \"express\").Router();\nconst adminRouter = () => {\n  router.get('/list-menu', (req, res) => {\n    res.render('pages/admin/menu/list-menu', { title: 'Daftar Menu' });\n  });\n\n  router.get('/create-menu', (req, res) => {\n    res.render('pages/admin/menu/create-menu', { title: 'Buat Menu' });\n  });\n\n  router.get('/detail-menu', (req, res) => {\n    res.render('pages/admin/menu/detail-menu', { title: 'Detail Menu', menuId: req.query.menuId });\n  });\n\n  router.get('/edit-menu', (req, res) => {\n    res.render('pages/admin/menu/edit-menu', { title: 'Perbarui Menu', menuId: req.query.menuId });\n  });\n\n  router.get('/customer/customer-edit', (req, res) => {\n    res.render('pages/admin/customer/customer-list', { title: 'Daftar Pelanggan' });\n  });\n\n  router.get('/user', (req, res) => {\n    res.render('pages/admin/user/user-list', { title: 'Daftar Pelanggan' });\n  });\n\n  router.get('/order', (req, res) => {\n    res.render('pages/admin/order/order-list', { title: 'Daftar Pesanan' });\n  });\n\n  return router;\n};\n\nmodule.exports = adminRouter;\n\n\n//# sourceURL=webpack://express-validator-tut/./src/routes/admin.router.js?");

/***/ }),

/***/ "./src/routes/index.js":
/*!*****************************!*\
  !*** ./src/routes/index.js ***!
  \*****************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("const adminRouter = __webpack_require__(/*! ./admin.router */ \"./src/routes/admin.router.js\");\n\nconst router = __webpack_require__(/*! express */ \"express\").Router();\n\nconst routes = () => {\n  router.get('/', (req, res) => {\n    res.render('pages/landing', { title: 'Foodie' });\n  });\n\n  router.get('/login', (req, res) => {\n    res.render('pages/login', { title: 'login' });\n  });\n\n  router.get('/register', (req, res) => {\n    res.render('pages/register', { title: 'Register' });\n  });\n\n  router.get('/menu', (req, res) => {\n    res.render('pages/menu', { title: 'Menu Page', categoryId: null });\n  });\n\n  router.get('/favorite', (req, res) => {\n    res.render('pages/menu', { title: 'Menu Page', categoryId: 'fav' });\n  });\n\n  router.get('/cart', (req, res) => {\n    res.render('pages/konfirmasi-pesanan', { title: 'Cart' });\n  });\n\n  router.get('/menu/category/:categoryId', (req, res) => {\n    res.render('pages/menu', { title: 'Menu Page', categoryId: req.params.categoryId });\n  });\n\n  router.get('/deskripsi', (req, res) => {\n    res.render('pages/deskripsi', { title: 'Deskripsi Makanan', menuId: req.query.menuId });\n  });\n\n  router.get('/about-us', (req, res) => {\n    res.render('pages/about-us', { title: 'About Us' });\n  });\n\n  router.get('/home', (req, res) => {\n    res.render('pages/landing', { title: 'Foodie' });\n  });\n\n  router.get('/profile', (req, res) => {\n    res.render('pages/profile', { title: 'Profil' });\n  });\n\n  router.put('/profile', (req, res) => {\n    res.render('pages/profile', { title: 'Profil' });\n  });\n\n  router.get('/list-menu', (req, res) => {\n    res.render('pages/admin/menu/list-menu', { title: 'Daftar Menu' });\n  });\n\n  router.get('/create-menu', (req, res) => {\n    res.render('pages/admin/menu/create-menu', { title: 'Buat Menu' });\n  });\n\n  router.get('/detail-menu', (req, res) => {\n    res.render('pages/admin/menu/detail-menu', { title: 'Detail Menu', menuId: req.query.menuId });\n  });\n\n  router.get('/edit-menu', (req, res) => {\n    res.render('pages/admin/menu/edit-menu', { title: 'Perbarui Menu', menuId: req.query.menuId });\n  });\n\n  router.get('/notes', (req, res) => {\n    res.render('pages/notes', { title: 'Tambah Catatan' });\n  });\n\n  router.get('/order-history', (req, res) => {\n    res.render('pages/order/order-history', { title: 'Riwayat Pesanan' });\n  });\n\n  router.get('/order-detail', (req, res) => {\n    res.render('pages/order/order-detail', { title: 'Rincian Pesanan', orderId: req.query.orderId });\n  });\n\n  router.use('/admin', adminRouter());\n\n  return router;\n};\n\nmodule.exports = routes;\n\n\n//# sourceURL=webpack://express-validator-tut/./src/routes/index.js?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("express");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/app.js");
/******/ 	
/******/ })()
;