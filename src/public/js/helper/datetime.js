const addZero = (d) => {
    if (d < 10) { d = "0" + d }
    return d;
}

const getDatetime = (datetime) => {
    const month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    const d = new Date(datetime)
    const date = addZero(d.getDate()) + ' ' + month[d.getMonth()] + ' ' + d.getFullYear()
    const time = addZero(d.getHours()) + ':' + addZero(d.getMinutes()) + ':' + addZero(d.getSeconds())
    return date + ' - ' + time
}