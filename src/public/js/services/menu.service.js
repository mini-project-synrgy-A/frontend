const reponseToken = JSON.parse(localStorage.getItem('token'));
var menuData = [];
var cartData = [];
let dataToken = '';

$(document).ready(async () => {
  $('#loader').show();
  scrollControl();
  const categoryId = $('.menus-container').data('category-id');
  await fetchCategory(categoryId ? categoryId : null);
  await renderMenu(categoryId ? categoryId : null);
  $('#loader').hide();

  let minPrice, maxPrice, search;

  $('#search-menu').on(
    'keyup',
    delay((event) => {
      search = $(event.target).val();
      removeMenu();
      renderMenu(categoryId ? categoryId : null, search, minPrice);
    }, 500)
  );

  $('#min-price').on(
    'keyup',
    delay((event) => {
      minPrice = $(event.target).val();

      removeMenu();
      renderMenu(categoryId ? categoryId : null, search, minPrice, maxPrice);
    }, 500)
  );

  $('#max-price').on(
    'keyup',
    delay((event) => {
      maxPrice = $(event.target).val();

      removeMenu();
      renderMenu(categoryId ? categoryId : null, search, minPrice, maxPrice);
    }, 500)
  );

  $('#btn-filter').on('click', () => {
    if ($('#btn-filter').attr('aria-expanded') === 'true') {
      $('#btn-filter').hide();
    }
  });

  // create cart
  $('button.btn-menu').click((event) => {
    // add cart if user is login
    if (reponseToken) {
      const menuId = $(event.target).data('menu-id');
      $('#add-order-' + menuId).hide();
      $('#btn-calc-' + menuId).show();
      handleCart(menuId, 1);
    } else {
      if(confirm('Please login first to continue')) {
        window.location.href = '/login';
      }
    }
  });

  // increase qty
  $('button.plus').click((event) => {
    const menuId = $(event.target).data('menu-id');
    const qty = parseInt($(event.target).prev().val());
    $(event.target)
      .prev()
      .val(qty + 1);
    handleCart(menuId, qty + 1);
  });

  // decrease qty
  $('button.minus').click((event) => {
    const menuId = $(event.target).data('menu-id');
    const qty = parseInt($(event.target).next().val());
    if (qty == 1) {
      $(event.target).parent().hide();
      $(event.target).parent().prev().show();
    } else {
      $(event.target)
        .next()
        .val(qty - 1);
    }
    handleCart(menuId, qty - 1);
  });

  $('.detail-menu').on('click', async function (e) {
    const menuId = $(this).data('id');
    e.preventDefault();
    await handleDetailMenu(menuId);
    getReview(menuId);
    $('#modal-detail-menu').modal('show').find('.modal-content');
  });
});

function handleCart(menuId, qty) {
  const selectedCartData = cartData.filter((cart) => cart.MenuId === menuId);
  const selectedMenu = menuData.filter((menu) => menu.id === menuId)[0];
  if (selectedCartData.length === 0) {
    cartData.push({
      MenuId: selectedMenu.id,
      price: selectedMenu.price * qty,
      quantity: qty,
    });
  } else {
    cartData.forEach((cart) => {
      if (cart.MenuId === menuId) {
        cart.quantity = qty;
        cart.price = selectedMenu.price * qty;
      }
    });
  }

  if (qty === 0) {
    cartData = cartData.filter((cart) => cart.MenuId !== menuId);
  }

  console.log(cartData);
  if (cartData.length > 0) {
    $('#confirmation-order').removeAttr('disabled');
  } else {
    $('#confirmation-order').attr('disabled', 'disabled');
  }
}

async function submitCreateCart() {
  try {
    $('#loader').show();
    await createCart({ carts: cartData });
    $('#loader').hide();
    window.location.href ='/cart';
  } catch (error) {
    console.log(error);
  }
}

function scrollControl() {
  let scroll_pos = 0;
  $(document).scroll(function () {
    scroll_pos = $(this).scrollTop();
    if (scroll_pos > 95) {
      $('.tool-section').addClass('nav-fixed');
      $('.menus-container').css('margin-top', '200px');
    } else {
      $('.tool-section').removeClass('nav-fixed');
      $('.menus-container').css('margin-top', '0');
    }
  });
}

const renderMenu = async (category = null, search = '', minPrice = null, maxPrice = null) => {
  try {
    menuData = await fetchMenu(category);

    if (search !== '') {
      menuData = menuData.filter((menu) => {
        return menu.name.toLowerCase().includes(search);
      });
    }

    if (minPrice) {
      menuData = menuData.filter((menu) => {
        return menu.price >= minPrice;
      });
    }

    if (maxPrice) {
      menuData = menuData.filter((menu) => {
        return menu.price <= maxPrice;
      });
    }

    const menuContainer = $('.menus-container');

    menuData.forEach((menu) => {
      menuContainer.append(generateMenuCard(menu));
    });

    $('.fav').hide();
    $('.btn-add-cart').hide();

    // activate cart and favorite feature if user is login
    if (reponseToken) {
      $('.fav').show();
      let favs = await getFavorite();
      favs = favs.map((item) => item.Menu);
      favsIds = favs.map((item) => item.id);

      $('.fav').each((i, obj) => {
        if (favsIds.includes($(obj).data('menu-id'))) {
          $(obj).html('<i class="bi bi-heart-fill" data-fav=true></i>');
        } else {
          $(obj).html('<i class="bi bi-heart" data-fav=false></i>');
        }
      });

      // favorite button
      $('.fav').click(async function (event) {
        try {
          const menuId = $(this).data('menu-id');
          if ($(event.target).data('menu-id') !== $(this).data('menu-id') && $(event.target).data('fav') === false) {
            const addFavUrl = BASE_PATH + '/favorite/add';
            let favRes = await fetch(addFavUrl, {
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + reponseToken.token,
              },
              body: JSON.stringify({ menu_id: menuId }),
              method: 'POST',
            });
            favRes = await favRes.json();

            if (favRes.status === 'created' || favRes.status === 'success') {
              $(event.target).parent().html('<i class="bi bi-heart-fill" data-fav=true></i>');
              $(event.target).find('i').prevObject.remove();
            }
          } else if (
            $(event.target).data('menu-id') !== $(this).data('menu-id') &&
            $(event.target).data('fav') === true
          ) {
            const delFavUrl = BASE_PATH + '/favorite/delete/' + menuId;
            let favRes = await fetch(delFavUrl, {
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + reponseToken.token,
              },
              method: 'DELETE',
            });
            favRes = await favRes.json();

            if (favRes.status === 'created' || favRes.status === 'success') {
              $(event.target).parent().html('<i class="bi bi-heart" data-fav=false></i>');
              $(event.target).find('i').prevObject.remove();
            }
          }
        } catch (err) {
          console.log(err);
        }
      });
    }

    const cart = await getDetailCart();
    if (cart.length > 0) {
      cartData = cart;
      $('#confirmation-order').removeAttr('disabled');
      cartData.forEach((cart) => {
        console.log(cart);
        $('#add-order-' + cart.MenuId).hide();
        $('#btn-calc-' + cart.MenuId).show();
        $('#cart-qty-' + cart.MenuId).val(cart.quantity);
      });
    }
  } catch (err) {
    console.log(err.message);
  }
};

const fetchCategory = async (categoryId) => {
  try {
    const url = BASE_PATH + '/menu-category/all';
    const response = await fetch(url, {
      method: 'GET',
    });
    const categories = await response.json();

    const tabPanel = $('.tab-panel');
    const nav = $('<nav></nav>', { class: 'nav' });

    if (categoryId === null) {
      nav.append(`<a class="nav-link category-active mx-1" aria-current="page" href="/menu">Seluruh menu</a>`);
    } else {
      nav.append(`<a class="nav-link category-inactive mx-1" aria-current="page" href="/menu">Seluruh menu</a>`);
    }

    if (reponseToken) {
      if (categoryId === 'fav') {
        nav.append(`<a class="nav-link category-active mx-1" aria-current="page" href="/favorite">Favorite</a>`);
      } else {
        nav.append(`<a class="nav-link category-inactive mx-1" aria-current="page" href="/favorite">Favorite</a>`);
      }
    }

    categories.data.forEach((category) => {
      if (category.id == categoryId) {
        nav.append(
          `<a class="nav-link category-active mx-1" aria-current="page" href="/menu/category/${category.id}">${category.categoryName}</a>`
        );
      } else {
        nav.append(
          `<a class="nav-link category-inactive mx-1" aria-current="page" href="/menu/category/${category.id}">${category.categoryName}</a>`
        );
      }
    });
    tabPanel.append(nav);
  } catch (err) {}
};

const fetchMenu = async (category) => {
  console.log(category);
  let menus, response;

  if (!category) {
    const url = BASE_PATH + '/menu/all';
    response = await fetch(url, {
      method: 'GET',
    });
    menus = await response.json();
    menus = menus.data;
  } else if (category === 'fav') {
    menus = await getFavorite();
    menus = menus.map((item) => item.Menu);
  } else {
    const url = BASE_PATH + '/menu-category/' + category;
    response = await fetch(url, {
      method: 'GET',
    });
    menus = await response.json();
    menus = menus.data;
  }

  return menus;
};

const generateMenuCard = (menu) => {
  const menuName = menu.name.length > 15 ? menu.name.substring(0, 15) + '...' : menu.name;
  const formatRupiah = (money) => {
    return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};
  return `<div class="col-3 mb-5">
            <div class="card border-0" style="width: 18rem;">
                <div class="d-flex justify-content-center">
                    <img src="${menu.imageUrl}" class="card-img-top detail-menu" alt="" data-id=${menu.id}>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between fs-5">
                        <p class="menu-name fw-bold detail-menu" data-id=${menu.id}>${menuName}</p>
                        <a class="btn btn-link fav" style="color:red" data-menu-id=${menu.id}>
                            <i class="bi bi-heart"></i>
                        </a>
                    </div>
                    <p class="menu-price">${formatRupiah(menu.price)}</p>
                    <div align="center">
                        <button type="submit" class="btn btn-primary btn-dark foodie-bg rounded-pill border-0 btn-menu py-3 px-5 "
                            data-menu-id=${menu.id} id=${'add-order-' + menu.id}>
                            Tambah Pesanan
                        </button>
                        <div class="btn-add-cart qty py-2"  id=${'btn-calc-' + menu.id}>
                            <button class="minus foodie-border bg-white" data-menu-id=${menu.id}>-</button>
                            <input type="text" class="count" name="qty" value="1" id=${'cart-qty-' + menu.id}>
                            <button class="plus foodie-border bg-white" data-menu-id=${menu.id}>+</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
};

const removeMenu = () => {
  const menuContainer = $('.menus-container');
  menuContainer.empty();
};

function delay(callback, ms) {
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

async function handleDetailMenu(menuId) {
  $('#loader').show();
  const detailMenu = await menuDetail(menuId);
  console.log(detailMenu);
  const formatRupiah = (money) => {
    return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};
  if (detailMenu) {
    $('.deskripsi-container').empty();
    $('.deskripsi-container').append(`
    <button type="button" class="btn-close exit-btn" aria-label="Close" data-bs-dismiss="modal" id="close-modal">
                        </button>
                        <div class="top">
                            <div class="left">
                                <h5 id="name">${detailMenu.name}</h5>
                                <div id="tags" class="tag-box">${detailMenu.Tags[0].name}</div>
                                <h5 id="price">Rp ${formatRupiah(detailMenu.price)}</h5>
                            </div>
                            <div class="right">
                                <img class="image" id="imageUrl" src =${detailMenu.imageUrl}>
                            </div>
                        </div>
                        <div class="description-body">
                            <div>
                                <h6>Deskripsi</h6>
                                <p id="description">${detailMenu.description}</p>
                            </div>
                            <div>
                                <p id="bestBefore" style="font-style: italic;">${detailMenu.bestBefore}</p>
                            </div>
                        </div>
                        <div class="review-container description-body">
                <h6>Reviews</h6>
                <div class="mt-3 reviews">
                </div>
                        </div>`);
                        
    $('#loader').hide();
  }
}
