$('.show-hide').click(function () {
  $(this).toggleClass('bi-eye bi-eye-slash');
  var input = $($(this).attr('toggle'));
  if (input.attr('type') == 'password') {
    input.attr('type', 'text');
  } else {
    input.attr('type', 'password');
  }
});

$('.show-hide-confirm').click(function () {
  $(this).toggleClass('bi-eye bi-eye-slash confirm');
  var input = $($(this).attr('toggle'));
  if (input.attr('type') == 'password') {
    input.attr('type', 'text');
  } else {
    input.attr('type', 'password');
  }
});

// retype password
var password = document.getElementById('password'),
  confirm_password = document.getElementById('confirm_password');
function validatePassword() {
  if (password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

//post json signup

$(document).ready(function () {
  const formRegistrasi = $('#form__registrasi');
  $('#form_message').hide();
  $('#loader').hide();
  formRegistrasi.on('submit', function (e) {
    // di klik submit / button login
    e.preventDefault();
    const firstname = $('#inputFirstName').val();
    const lastname = $('#inputLastName').val();
    const username = $('#inputUsername').val();
    const password = $('#password').val();
    register(firstname, lastname, username, password);
  });
});

async function register(fname, lname, name, pass) {
  $('#loader').show();
  const response = await fetch(BASE_PATH + '/users/register', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'post',
    body: JSON.stringify({
      firstname: fname,
      lastname: lname,
      username: name,
      password: pass,
    }),
  });

  const data = await response.json();
  $('#loader').hide();
  if (data.status === 'error' || data.status === 'forbidden') {
    $('#form_message').show();
    $('#message').html(data.message);
  } else {
    window.location.href = '/login';
  }
}
