let image = '';

$(document).ready(function () {
  profile();

  const formUpdate = $('#form_update');
  formUpdate.on('submit', function (e) {
    e.preventDefault();
    const firstname = $('#first-name').val();
    const lastname = $('#last-name').val();
    const address = $('#address').val();
    const phone = $('#phone').val();
    // const imageUrl = $('#profileImage').val();

    updateProfile(firstname, lastname, address, phone, image);
  });

  $("#upload-img-btn").click(function(e) {
    e.preventDefault();
    $("#imageUpload").click();
  });

  $("#imageUpload").change(function(){
        readURL(this);
        //other uploading proccess [server side by ajax and form-data ]
    });
});

async function profile() {
  const token = JSON.parse(localStorage.getItem('token'));
  const response = await fetch(BASE_PATH + '/profiles/detail', {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token.token,
    },
    method: 'get',
  });

  const respond = await response.json();

  let data = {};

  console.log(respond.data);

  if (respond.status === 'error' || respond.status === 'forbidden') {
    console.error(respond.message);
  } else {
    data = respond.data;
    $('#first-name').val(data.firstname);
    $('#last-name').val(data.lastname);
    $('#address').val(data.address);
    $('#phone').val(data.phone);
    $('#profileImage').html(`<img id="avatar" src=${data.imageUrl} />`);
  }
}

async function updateProfile(firstname, lastname, address, phone, image) {
  const token = JSON.parse(localStorage.getItem('token'));
  // console.log('token', token);
  // const response = await fetch(BASE_PATH + '/profiles/update', {
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Authorization: 'Bearer ' + token.token,
  //   },
  //   method: 'put',
  //   body: JSON.stringify({
  //     firstname: firstname,
  //     lastname: lastname,
  //     address: address,
  //     phone: phone,
  //     imageUrl: imageUrl,
  //   }),
  // });

  // const result = await response.json();

  // console.log(result.data);

  // if (result.status === 'error' || result.status === 'forbidden') {
  //   console.error(result.message);
  // } else {
  //   data = result.data;
  //   console.log('imageUrl::', imageUrl);
  //   window.location.href = '/'
  // }
  var myHeaders = new Headers();
  myHeaders.append("authorization", "Bearer " + token.token);

  var formdata = new FormData();
  formdata.append("firstname", firstname);
  formdata.append("lastname", lastname);
  formdata.append("address", address);
  formdata.append("phone", phone);
  if (image) {
    formdata.append("image", image, image.name);
  }

  var requestOptions = {
      method: 'put',
      headers: myHeaders,
      body: formdata,
      redirect: 'follow'
  };

  const response = await fetch(BASE_PATH + '/profiles/update', requestOptions)

  const result = await response.json();

  console.log(result.data);

  if (result.status === 'error' || result.status === 'forbidden') {
    console.error(result.message);
  } else {
    data = result.data;
    console.log('imageUrl::', image);
    window.location.href = '/'
  }
}

function readURL(input) {
  image = input.files[0]
  if (input.files && image) {
      var reader = new FileReader();
      reader.onload = function (e) {
        e.preventDefault();
        $('#avatar').attr('src', 
          window.URL.createObjectURL(image),
          e.target.result);
      }
      
      reader.readAsDataURL(image);
  }
};