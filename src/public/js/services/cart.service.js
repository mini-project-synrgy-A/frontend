const reponseToken = JSON.parse(localStorage.getItem('token'));
let dataToken = '';
let dataProfile = '';
let dataCarts = '';
let totalPrice = 0;
let deletedCart = '';

$(document).ready(function () {
  if (!reponseToken.token) {
    window.location.href = '/login';
  } else {
    return;
  }
});

async function run() {
  await showData();
}

async function showData() {
  $('#loader').show();

  if (dataCarts.length === 0) {
    $('#order').attr('disabled', 'disabled');
    $('#add-item').attr('disabled', 'disabled');
  }

  await checkToken();
  await getProfile();
  dataCarts = await getDetailCart();

  if (dataCarts.length > 0) {
    $('#order').removeAttr('disabled');
    $('#add-item').removeAttr('disabled');
  }

  if (dataCarts.length == 0) {
    window.location.href = '/menu';
  } else if (!dataProfile.data.address) {
    alert('Update your profile to continue')
    window.location.href = '/profile'
  }
  const alamat = $('#alamat');
  alamat.val(dataProfile.data.address);
  displayCart();
  setTotal();
  $('#loader').hide();
  $('.catatan-item').hide();
  $('.cancel-catatan').hide();

  $('.btn-catatan-item').on('click', function (e) {
    e.preventDefault();
    $(this).prev().show();
    console.log('catatan: ' + $(this).data('id'));
    $(this).hide()
    $(this).next().show(); // show .cancel-catatan
  });

  $('.cancel-catatan').on('click', function (e) {
    e.preventDefault();
    $(this).prev().prev().val(''); // clear catatan input form
    $(this).prev().prev().hide();
    $(this).hide();
    $(this).prev().show(); // show .btn-catatan-item
  })
}

const addZero = (d) => {
  if (d < 10) { d = "0" + d }
  return d;
}

const getDate = () => {
  const d = new Date()
  console.log(d.getMonth())
  const date = d.getFullYear() + '-' + addZero(d.getMonth() + 1) + '-' + addZero(d.getDate())
  return date
}

function displayCart() {
  const currDate = getDate()
  console.log(currDate)
  const orderList = $('#order-list');
  orderList.empty();
  dataCarts.forEach(async (cart) => {
    orderList.append(`
          <div class="row mb-5">
            <div class="col-2">
              <img src="${cart.Menu.imageUrl}" alt="" />
            </div>
            <div class="col-8">
              <p class="menuName">${cart.Menu.name}</p>
              <textarea type="text" class="catatan-item" data-id=${cart.id} placeholder="Tambahkan catatan item"></textarea>
              <a class="btn foodie-btn-border btn-catatan-item" data-id=${cart.id}>Catatan</a>
              <a class="btn foodie-btn-border cancel-catatan" data-id=${cart.id}>Batalkan</a>
              <form class="mt-2">
                <div class="decrease-btn btn" id="decrease" onclick="decreaseValue(${cart.id})" value="Decrease Value"> 
                -
                </div>
                <input type="number" id="qty-${cart.id}" class="qty" value="${cart.quantity}" data-menu-id=${cart.MenuId} />
                <div class="increase-btn btn" id="increase" onclick="increaseValue(${cart.id})" value="Increase Value">
                  +
                </div>
              </form>
              <div class="mt-2">
                Tanggal Pengiriman : <input type="date" class="delivery-date" value=${currDate} data-id=${cart.id}>
              </div>
            </div>
            <div class="col-2">
              <p id="harga">
                <span id="qty-item-${cart.id}">${cart.quantity}</span> 
                x 
                <span id="price-item-${cart.id}" data-price=${cart.Menu.price}>${formatRupiah(cart.Menu.price)}</span>
              </p>
              <p id="harga">
                ( <span class="total-harga" id="amount-${cart.id}" data-price=${cart.price}>${formatRupiah(cart.price)}</span> )
                </p>
            </div>
          </div>
        `);
  });
}

async function addItem() {
  const payloadCreate = [];
  dataCarts.forEach((cart) => {
    payloadCreate.push({
      MenuId: cart.MenuId,
      price: cart.price,
      quantity: cart.quantity,
    });
  });
  $('#loader').show();
  await createCart({ carts: payloadCreate });
  $('#loader').hide();
  window.location.href = '/menu';
}

function setTotal() {
  let totalPrice = 0;
  for (let i = 0; i < dataCarts.length; i++) {
    totalPrice = totalPrice + dataCarts[i].price;
  }
  $('#totalPrice').data('total-price', totalPrice);
  $('#totalPrice').html(`${formatRupiah(totalPrice)}`);
}

async function checkToken() {
  const response = await fetch(BASE_PATH + '/users/check', {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + reponseToken.token,
    },
    method: 'get',
  });

  dataToken = await response.json();
}

async function getProfile() {
  const url = BASE_PATH + '/profiles/detail/'; /*+ dataToken.data.id*/
  const response = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + reponseToken.token,
    },
    method: 'get',
  });

  dataProfile = await response.json();
}

function updateDisplayPrice(cartId, value, price) {
  const amount = value * parseInt(price)
  $('#qty-item-' + cartId).text(value);
  $('#amount-' + cartId).text(formatRupiah(amount));
  $('#amount-' + cartId).data('price', amount);
}

function increaseValue(cartId) {
  let value = parseInt(document.getElementById('qty-' + cartId).value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('qty-' + cartId).value = value;
  
  const price = $('#price-item-' + cartId).data('price')
  updateDisplayPrice(cartId, value, price);
  
  let newTotal = $('#totalPrice').data('total-price');
  console.log('plus: ' + newTotal + '+' + price);
  newTotal += price
  $('#totalPrice').data('total-price', newTotal);
  $('#totalPrice').html(formatRupiah(newTotal));
  
  handleCart(cartId, value);
}

function decreaseValue(cartId) {
  let value = parseInt(document.getElementById('qty-' + cartId).value, 10);
  value = isNaN(value) ? 0 : value;
  value < 1 ? (value = 1) : '';
  value--;
  document.getElementById('qty-' + cartId).value = value;

  const price = $('#price-item-' + cartId).data('price');
  updateDisplayPrice(cartId, value, price);
  
  let newTotal = $('#totalPrice').data('total-price');
  console.log('minus: ' + newTotal + '-' + price);
  newTotal -= price
  $('#totalPrice').data('total-price', newTotal);
  $('#totalPrice').html(formatRupiah(newTotal));
  
  handleCart(cartId, value);
}

async function handleCart(cartId, qty) {
  dataCarts.forEach((cart) => {
    if (cart.id === cartId) {
      cart.quantity = qty;
      cart.price = cart.Menu.price * qty;
    }
  });

  console.log(qty);
  if (qty === 0) {
    dataCarts = dataCarts.filter((cart) => cart.id !== cartId);
    displayCart();
  }
  console.log(dataCarts);

  if (dataCarts.length === 0) {
    $('#loader').show();
    await deleteAllUserCart();
    $('#loader').hide();

    window.location.href = '/menu';
  }
}

async function confirmOrder() {
  try {
    let items = [];
    dataCarts.forEach(async (cart) => {
      const itemNote =  $(`.catatan-item[data-id=${cart.id}]`).val()
      const deliveryDate =  $(`.delivery-date[data-id=${cart.id}]`).val()
      items.push(
        JSON.stringify({
          MenuId: cart.MenuId,
          price: cart.Menu.price,
          quantity: cart.quantity,
          note: itemNote,
          deliveryDate: deliveryDate
        })
      );
    });
    const data = {
      // coupon: coupon,
      // note: note,
      deliveryAddress: $('#alamat').val(),
      note: $('#order-note').val(),
      items: items,
    };
    console.log(data);
    
    $('#loader').show();
    await deleteAllUserCart();
    const order = await createOrder(data);
    $('#loader').hide();
    console.log(order);
    window.location.href = '/order-history';
  } catch (error) {
    console.log(error);
  }
}

run();

$('#order').click(async function () {
  try {
    await confirmOrder();
  } catch (error) {
    console.log(error);
  }
});