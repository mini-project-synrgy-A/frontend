async function getMenu(req, res) {
    const response = await fetch(BASE_PATH + '/menu/all', {
    method: 'get',
    });
    const respond = await response.json();

    if (respond.status === 'error') {
        console.error(respond.message);
    } else {
        let n_data = respond.data.length;
        let i = 0;
        let $carouselItemMenu = $(`
            <div class="carousel-item">
                <div class="row">
                </div>
            </div>>  
            `);

        respond.data.forEach((data) => {
            
            const menuName = data.name.length > 12 ? data.name.substring(0, 12) + "..." : data.name;
            const menuDesc = data.description.length > 50 ? data.description.substring(0, 50) + "..." : data.description;
            
            if (i === 0) {
                $carouselItemMenu.addClass('active');
            } else if (i > 4) {
                $carouselItemMenu.removeClass('active');
            }
            if (i % 4 === 0) {
                $carouselItemMenu = $carouselItemMenu.clone().empty();
                $carouselItemMenu.append(`<div class="row"></div>`)
            }
            $carouselItemMenu.find('.row').append(
                `
                <div class="col">
                    <div class="menu-card-container">
                    <img src=${data.imageUrl} class="d-block w-100" alt="..."/>
                    <div class="menu-card-detail">
                        <h5 class="fw-bold">${menuName}</h5>
                        <p>${menuDesc}</p>
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-dark modal-btn" data-id=${data.id}>Selengkapnya<i class="bi bi-arrow-right"></i></a>
                        </div>
                    </div>
                    </div>
                </div>
            `)
            if (i % 4 === 0 || (i === n_data - 1)) {
                $('.menu-carousel').append($carouselItemMenu);
            }

            i++;
        })

        // $('.modal-btn').on('click', function (e) {
        //     e.preventDefault();
        //     $('#modal-deskripsi').modal('show').find('.modal-content').load($(this).attr('href'));
        // });

        $('.modal-btn').on('click', async function (e) {
            const menuId = $(this).data('id');
            e.preventDefault();
            getReview(menuId);
            await handleDescMenu(menuId);
            $('#modal-deskripsi').modal('show').find('.modal-content').load($(this).attr('href'));
        });
    }
}

async function handleDescMenu(menuId) {
  $('#loader').show();
  const detailMenu = await menuDetail(menuId);
  console.log(detailMenu);
  const formatRupiah = (money) => {
    return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};
  if (detailMenu) {
    $('.deskripsi-container').empty();
    $('.deskripsi-container').append(`
            <button type="button" class="btn-close exit-btn" aria-label="Close" data-bs-dismiss="modal" id="close-modal">
                        </button>
                        <div class="top">
                            <div class="left">
                                <h5 id="name">${detailMenu.name}</h5>
                                <div id="tags" class="tag-box">${detailMenu.Tags[0].name}</div>
                                <h5 id="price">Rp ${formatRupiah(detailMenu.price)}</h5>
                            </div>
                            <div class="right">
                                <img class="image" id="imageUrl" src =${detailMenu.imageUrl}>
                            </div>
                        </div>
                        <div class="description-body">
                            <div>
                                <h6>Deskripsi</h6>
                                <p id="description">${detailMenu.description}</p>
                            </div>
                            <div>
                                <p id="bestBefore" style="font-style: italic;">${detailMenu.bestBefore}</p>
                            </div>
                        </div>
                        <div class="review-container description-body">
                            <h6>Reviews</h6>
                            <div class="mt-3 reviews"></div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a class="btn btn-primary btn-dark foodie-bg border-0 rounded-pill pt-2 pb-2 ps-3 pe-3 center" type="button" id="order" href="/menu">Pesan
                        Sekarang</a>
                        </div>
            </div>`);
                        
    $('#loader').hide();
  }
}

$(document).ready(function () {
    getMenu();

    $('#kacang').on('click', function (e) {
        e.preventDefault();
        $('#funfact-kacang').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('#bayam').on('click', function (e) {
        e.preventDefault();
        $('#funfact-bayam').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('#telur').on('click', function (e) {
        e.preventDefault();
        $('#funfact-telur').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('#buncis').on('click', function (e) {
        e.preventDefault();
        $('#funfact-buncis').modal('show').find('.modal-content').load($(this).attr('href'));
    });
});

