let currentOrderId;

async function handleAllOrder() {
  $('#loader').show();
  const orderData = await getAllOrder();
  if (orderData.length > 0) {
    $('.row-user').empty();
    orderData.forEach((order, index) => {
      $('.row-order').append(`<tr>
              <td>${index + 1}</td>
              <td>${order.User?.Profile?.firstname}</td>
              <td>${order.User?.Profile?.lastname}</td>
              <td>${localDate(order.createdAt)}</td>
              <td>${order.paidAt ? localDate(order.paidAt) : '-'}</td>
              <td>${formatRupiah(order.amountPaid)}</td>
              <td>${order.deliveryAddress}</td>
              <td>${order.note}</td>
              <td class="text-capitalize ${
                order.status === 'done'
                  ? 'text-success'
                  : order.status === 'process' && order.paidAt === null
                  ? 'text-warning'
                  : order.status === 'process' && order.paidAt !== null
                  ? 'text-primary'
                  : 'text-danger'
              }">${order.paidAt === null || order.status === 'done' ? order.status : 'paid'}</td>
              <td>
                <div class="d-flex">
                    <button class="m-1 btn btn-warning detail-order" data-id=${order.id}>
                        <i class="bi bi-eye-fill"></i>
                    </button>
                    ${
                      order.status === 'process' && order.paidAt === null
                        ? `<button class="m-1 btn btn-primary edit-status-order" data-id=${order.id}>
                            <i class="bi bi-pencil-fill"></i>
                        </button>`
                        : ''
                    }
                </div>
              </td>
          </tr>`);
    });
    $('#loader').hide();
  }
}

async function handleDetailOrder(orderId) {
  $('#loader').show();
  const orderData = await getDetailOrder(orderId);
  console.log(orderData);
  if (orderData) {
    $('.row-detail-order').empty();
    orderData.ItemOrders.forEach((order, index) => {
      $('.row-detail-order').append(`<tr>
              <td>${index + 1}</td>
              <td><img src=${order.Menu?.imageUrl} style="width: 150px; height:150px; object-fit:cover;"></td>
              <td>${order.Menu?.name}</td>
              <td>${formatRupiah(order.price)}</td>
              <td>${order.quantity}</td>
              <td>${order.note}</td>
          </tr>`);
    });
    $('#loader').hide();
  }
}

$(document).ready(async function () {
  //   $('.alert').alert('close');
  $('.alert').hide();
  await handleAllOrder();

  $('.delete-order').click(async function (e) {
    $('#loader').show();
    const orderId = $(this).data('id');
    await deleteOrder(orderId);
    await handleAllorder(2);
    $('#loader').hide();
  });

  $('.detail-order').on('click', async function (e) {
    const orderId = $(this).data('id');
    e.preventDefault();
    await handleDetailOrder(orderId);
    $('#modal-order').modal('show').find('.modal-content');
  });

  $('.edit-status-order').on('click', async function (e) {
    const orderId = $(this).data('id');
    e.preventDefault();
    $('#loader-modal').hide();
    $('#modal-edit-status').data('order-id', orderId);
    $('#modal-edit-status').modal('show').find('.modal-content');
  });

  $('#update-status').on('click', async function (e) {
    const selectedValue = $('#status-value').val();
    const orderId = $('#modal-edit-status').data('order-id'); //getter
    $('#loader-modal').show();
    if (selectedValue === 'paid') {
      await paidOrder(orderId);
    } else {
      await cancelOrder(orderId);
    }
    $('#modal-edit-status').modal('hide');
    await handleAllOrder();
    $('.alert').show();
    setTimeout(function () {
      $('.alert').hide();
    }, 2000);
  });
});
