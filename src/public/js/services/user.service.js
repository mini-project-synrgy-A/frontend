async function handleAllUser(roleId) {
  if (roleId === 1) {
    $('#admin').removeClass('category-inactive').addClass('category-active');
    $('#pelanggan').removeClass('category-active').addClass('category-inactive');
  } else {
    $('#admin').removeClass('category-active').addClass('category-inactive');
    $('#pelanggan').removeClass('category-inactive').addClass('category-active');
  }

  $('#loader').show();
  const userData = await getAllUser(roleId);
  if (userData.length > 0) {
    if(roleId === 1){
      $('.row-user').empty();
      $('thead').empty()
      $('thead').append(
                        `<th>No</th>
                        <th>Username</th>
                        <th>Tanggal Registrasi</th>
                        <th>Aksi</th>`
      )
      userData.forEach((user, index) => {
        $('.row-user').append(`<tr>
                                  <td>${index + 1}</td>
                                  <td>${user.username}</td>
                                  <td>${user.createdAt? getDatetime(user.createdAt) : ''}</td>
                                  <td>
                                      <button class="btn btn-danger delete-user" data-id=${user.id}>
                                          <i class="bi bi-trash-fill"></i>
                                      </button>
                                  </td>
                              </tr>`);
      });
      $('#loader').hide();

      $('.delete-user').click(async function (e) {
        const userId = $(this).data('id');
        updateDisplay(userId)
      });
    }else{
      $('.row-user').empty();
      $('thead').empty()
      $('thead').append(
                        `<th>No</th>
                        <th>Nama Depan</th>
                        <th>Nama Belakang</th>
                        <th>Alamat</th>
                        <th>No. Telepon</th>
                        <th>Tanggal Registrasi</th>
                        <th>Aksi</th>`
      )
      userData.forEach((user, index) => {
        $('.row-user').append(`<tr>
                                  <td>${index + 1}</td>
                                  <td>${user.Profile?.firstname}</td>
                                  <td>${user.Profile?.lastname}</td>
                                  <!-- <td>${user.Profile?.birthDate}</td> -->
                                  <!-- <td>${user.Profile?.occupation}</td> -->
                                  <td>${user.Profile?.address}</td>
                                  <td>${user.Profile?.phone}</td>
                                  <td>${user.createdAt? getDatetime(user.createdAt) : ''}</td>
                                  <!-- <td>${user.Profile?.email}</td> -->
                                  <td>
                                      <button class="btn btn-danger delete-user" data-id=${user.id}>
                                          <i class="bi bi-trash-fill"></i>
                                      </button>
                                  </td>
                              </tr>`);
      });
      $('#loader').hide();

      $('.delete-user').click(async function (e) {
        const userId = $(this).data('id');
        updateDisplay(userId)
      });
    }
  }
}

$(document).ready(async function () {
  await handleAllUser(2);
});

const updateDisplay = async (userId) => {
  $('#loader').show();
  await deleteUser(userId);
  await handleAllUser(2);
  $('#loader').hide();
}