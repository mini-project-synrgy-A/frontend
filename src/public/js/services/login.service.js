$(document).ready(async function () {
  const token = JSON.parse(localStorage.getItem('token'));
  if (token) {
    window.location.href = '/profile';
  }
  const formLogin = $('#form__login');
  $('#loader').hide();
  $('#form_message').hide();
  formLogin.on('submit', async function (e) {
    e.preventDefault();
    const username = $('input[name=username]').val();
    const password = $('input[name=password]').val();
    doLogin(username, password);
  });
});

$('.show-hide').click(function () {
  $(this).toggleClass('bi-eye bi-eye-slash');
  var input = $($(this).attr('toggle'));
  if (input.attr('type') == 'password') {
    input.attr('type', 'text');
  } else {
    input.attr('type', 'password');
  }
});

async function doLogin(username, password) {
  try {
    $('#loader').show();
    const result = await authLogin(username, password);
    console.log(result);
    if (result.status === 'error' || result.status === 'forbidden') {
      $('#form_message').show();
      $('#message').html(result.message);
    } else {
      await asyncLocalStorage.setItem('token', JSON.stringify(result.data));
      const userData = await getUserDetail();
      localStorage.setItem('roleId', userData.RoleId);
      window.location.href = '/';
    }
    $('#loader').hide();
  } catch (error) {
    console.log(error);
  }
}

const asyncLocalStorage = {
  setItem: function (key, value) {
    return Promise.resolve().then(function () {
      localStorage.setItem(key, value);
    });
  },
};
