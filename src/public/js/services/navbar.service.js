$(document).ready(async function () {
  const pathname = window.location.pathname;
  $(`a[href$='${pathname}']`).addClass('nav-active');
  const token = JSON.parse(localStorage.getItem('token'));
  const roleId = JSON.parse(localStorage.getItem('roleId'));

  if (!token) {
    menuPublic();
  } else {
    if (roleId === 1) {
      menuAdmin();
    } else {
      menuCustomer();

      const url = BASE_PATH + "/profiles/detail";
      response = await fetch(url, {
        method: "GET",
        headers: {
          'Content-Type' : 'application/json',
          Authorization : 'Bearer ' + token.token, 
      },
      });
      const profile = await response.json();
      const DEFAULT_PROFILE_IMG = "https://synrgy-img.s3.ap-southeast-1.amazonaws.com/image/profile/default.jpg";
      let imageUrl = profile.data.imageUrl;

      if (imageUrl === null || imageUrl === '')
        imageUrl = DEFAULT_PROFILE_IMG;

      $('#profile-pic').attr('alt', profile.data.firstname + ' ' + profile.data.lastname);
      $('#profile-pic').attr('src', imageUrl);
    }
  }
});

function menuAdmin() {
  $('#nav-about').hide();
  $('#nav-menu').hide();
  $('#nav-login').hide();
  $('#nav-order-history').hide();

  $('#nav-profile-pic').show();
  $('#nav-list-menu').show();
  $('#nav-logout').show();
  $('#nav-order').show();
}

function menuCustomer() {
  $('#nav-about').show();
  $('#nav-menu').show();
  $('#nav-profile-pic').show();
  $('#nav-logout').show();
  $('#nav-order-history').show();

  $('#nav-login').hide();
  $('#nav-customer').hide();
  $('#nav-order').hide();
  $('#nav-list-menu').hide();
}

function menuPublic() {
  $('#nav-about').show();
  $('#nav-menu').show();
  $('#nav-login').show();

  $('#nav-order-history').hide();
  $('#nav-profile-pic').hide();
  $('#nav-logout').hide();
  $('#nav-customer').hide();
  $('#nav-order').hide();
  $('#nav-list-menu').hide();
}

function logout() {
  localStorage.clear();
  window.location.reload();
  window.location.href = '/';
}
