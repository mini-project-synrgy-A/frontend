const reponseToken = JSON.parse(localStorage.getItem("token"));

const addZero = (d) => {
    if (d < 10) { d = "0" + d }
    return d;
}

const getDatetime = (datetime) => {
    const month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    const d = new Date(datetime)
    const date = addZero(d.getDate()) + ' ' + month[d.getMonth()] + ' ' + d.getFullYear()
    const time = addZero(d.getHours()) + ':' + addZero(d.getMinutes()) + ':' + addZero(d.getSeconds())
    return date + ' - ' + time
}

const formatMoney = (money) => {
  return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

async function getOrder() {
    const orderData = await getAllOrder();

    if (orderData.length > 0) {
        console.log(orderData)
        orderData.forEach((data) => {
            const createdAt = getDatetime(data.createdAt)
            const paid = (data.paidAt) ? {status: "DIBAYAR", class: "paid"} : {status: "BELUM DIBAYAR", class: "notpaid"};
            $('.riwayat-pesanan').append(`<div class="row my-3 ${paid.class}">
                                            <div class="col">
                                                <p class="fw-bold m-0">${createdAt}</p>
                                                <br><br>
                                                <span class="fw-bold">Rp. ${formatMoney(data.amountPaid)}</span>
                                            </div>
                                            <div class="col text-end">
                                                <p class="fw-bold">${paid.status}</p><br>
                                                <a href="/order-detail?orderId=${data.id}" class="btn btn-primary btn-dark foodie-bg rounded-pill border-0 btn-menu py-2 px-4">
                                                    Detail Pesanan</a>
                                            </div>
                                        </div>`)
        })
    }
}

$(document).ready(function () {
    getOrder();
});