const reponseToken = JSON.parse(localStorage.getItem("token"));
let rate = 0;

const formatMoney = (money) => {
    return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

const addZero = (d) => {
    if (d < 10) { d = "0" + d }
    return d;
}

const getDate = (datetime) => {
    const month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    const d = new Date(datetime)
    const date = addZero(d.getDate()) + ' ' + month[d.getMonth()] + ' ' + d.getFullYear()
    return date
}

async function getMenu() {
    const response = await fetch(BASE_PATH + '/menu/all', {
    method: 'get',
    });
    const respond = await response.json();

    const resCategory = await fetch(BASE_PATH + '/menu-category/all', {
        method: 'get',
        });
    const categories = await resCategory.json();

    if (respond.status === 'error') {
        console.error(respond.message);
    } else {
        return respond.data
    }
}

const getOrder = async (orderId) => {
    const orderData = await getDetailOrder(orderId);
    const menus = await getMenu();
    console.log(orderData);
    if (orderData) {
        const reviews = await getReview(orderId);
        console.log(reviews)
        const isPaid = (orderData.paidAt) ? {status: "DIBAYAR", class: "is-paid"} : {status: "BELUM DIBAYAR", class: "is-notpaid"};
        $('#address').val(orderData.deliveryAddress)
        $('#order-note').val(orderData.note)
        orderData.ItemOrders.forEach((item) => {
            const menu = menus.filter((i) => i.id == item.MenuId)[0]
            const note = (item.note) ? item.note : '';
            $('.pesanan').append(`<div class="row mb-4">
                                    <div class="col-2">
                                        <img src="${menu.imageUrl}" alt="Nasi Kuning" />
                                    </div>
                                    <div class="col-8">
                                        <div class="row">
                                            <div class="col-6">
                                                <h5>${menu.name}</h5>
                                                <p class="mb-1">${item.quantity} x Rp. ${formatMoney(item.price)}</p>
                                                <p class="text-muted">Catatan : ${note}</p>
                                                <p>Tanggal Pengiriman : ${getDate(item.deliveryDate)}</p>
                                            </div>
                                            <div class="col-6">
                                                <div class="content-review" id="review-${item.id}">
                                                    <h5>Ulasan</h5>
                                                    <div class="mb-1" id="rate-review-${item.id}"></div>
                                                    <p id="item-review-${item.id}"></p>
                                                </div>
                                                <button class="btn btn-primary foodie-btn rounded-pill border-0 btn-review" style="display: none;"
                                                    data-order-id=${orderData.id} data-item-id=${item.id} data-menu-id=${item.MenuId}>
                                                        Beri Ulasan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <h5 class="text-end">Rp. ${formatMoney(item.quantity * item.price)}</h5>
                                    </div>
                                </div>`)
            
            $('#review-' + item.id).hide()
            // $(`button[data-item-id='${item.id}']`).show()

            if(reviews) {
                const review = reviews.filter((i) => i.MenuId == item.MenuId)[0]
                if (review) {
                    displayReview(item.id, review.review, review.rate)
                } else {
                    $(`button[data-item-id=${item.id}]`).show()
                }
            } else if (orderData.status == 'done'){
                $('.btn-review').show()
            }
        })

        $('.total-harga').append(`<h5 class=${isPaid.class}>${isPaid.status}</h5>
                                <h5 class="fw-bold">Total Harga: Rp. ${formatMoney(orderData.amountPaid)}</h5>`)
        
        if(orderData.status == 'done') {
            $('#btn-done').attr('disabled', 'disabled');
        } else if(isPaid.status === 'DIBAYAR') {
            $('#btn-done').removeAttr('disabled');
        }
    }

    // $('.btn-review').hide()
    $('.btn-review').on('click', async function (e) {
        e.preventDefault();
        $('#review').modal('show').find('.modal-content');
        $('#confirm-review').data('order-id', $(this).data('order-id'))
        $('#confirm-review').data('item-id', $(this).data('item-id'))
        $('#confirm-review').data('menu-id', $(this).data('menu-id'))
    });

    $('#loader').hide()
}

const displayReview = async (itemId, text, rate) => {
    $('#review-' + itemId).show()
    $('#item-review-' + itemId).html(text)
    displayRate(itemId, rate)
    $(`button[data-item-id=${itemId}]`).hide();
}

const displayRate = async (itemId, rate) => {
    const divRate = document.getElementById('rate-review-' + itemId)
    for (let i = 0; i < 5; i++) {
        if (i < rate) {
            const span = document.createElement("span")
            const star = document.createElement("i")
            star.classList.add("bi");
            star.classList.add("bi-star-fill");
            span.appendChild(star)
            divRate.appendChild(span);
        } else {
            const span = document.createElement("span")
            const star = document.createElement("i")
            star.classList.add("bi");
            star.classList.add("bi-star");
            span.appendChild(star)
            divRate.appendChild(span);
        }
    }
}

const tambahReview = async () => {
    const itemId = $('#confirm-review').data('item-id')
    const orderId = $('#confirm-review').data('order-id')
    const menuId = $('#confirm-review').data('menu-id')
    const textReview = $('#text-review')

    const data = {
        MenuId: menuId,
        OrderId: orderId,
        rate: rate,
        review: textReview.val()
    }
    // console.log(data)

    // fetch post createReview
    const res = await createReview(data)
    console.log("fetch create review: " + res)
    
    // $('#item-review-' + itemId).html()
    // $('#review-' + itemId).show()
    // displayRate(itemId, rate)
    // $(`button[data-item-id=${itemId}]`).hide();
    displayReview(itemId, textReview.val(), rate)

    rate = 0    
    textReview.val('') // reset text review
    $(':radio').prop('checked', false); // reset rate
    $('#review').modal('hide');
}

//post json
$(document).ready(function () {
    $('.pesanan').html();
    $('.total-harga').html();
    const orderId = $('.detail').data('id');
    console.log(orderId);
    getOrder(orderId);

    $(':radio').change(function() {
        console.log('New star rating: ' + this.value);
        rate = this.value
    });

    $('#btn-done').click(async function() {
        if(confirm('Konfirmasi pesanan telah diterima?')) {
            $('#btn-done').attr('disabled', 'disabled');
            $('.btn-review').show()

            const orderId = $('.detail').data('id');
            const done = await setDoneOrder(orderId)
            console.log(done)
        }
    })
});