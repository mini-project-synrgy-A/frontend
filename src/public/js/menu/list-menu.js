const reponseToken = JSON.parse(localStorage.getItem("token"));

async function deleteMenu(menuId) {
    const response = await fetch(BASE_PATH + '/menu/delete/' + menuId, {
        headers: {
            Authorization : 'Bearer ' + reponseToken.token, 
        },
        method: 'delete',
    });
    const respond = await response.json();

    if (respond.status === 'error') {
        console.error(respond.message);
    } else {
        console.log(menuId)
        console.log(respond.data)
        alert(respond.data.message)
        window.location.href = '/admin/list-menu';
    }
}

async function getMenu() {
    $('#loader').show();
    const response = await fetch(BASE_PATH + '/menu/all', {
    method: 'get',
    });
    const respond = await response.json();

    const resCategory = await fetch(BASE_PATH + '/menu-category/all', {
        method: 'get',
        });
    const categories = await resCategory.json();

    if (respond.status === 'error') {
        console.error(respond.message);
    } else {
        respond.data.forEach((data, index) => {
            // const menuName = data.name.length > 12 ? data.name.substring(0, 12) + "..." : data.name;
            const category = categories.data.filter((cat) => cat.id == data.MenuCategoryId)
            $('.row-menu').append(`<tr>
                                        <td>${index + 1}</td>
                                        <td>${category[0].categoryName}</td>
                                        <td>${data.name}</td>
                                        <td>${formatRupiah(data.price)}</td>
                                        <td>
                                            <p>${data.description}<p>
                                            <p class="fst-italic">${data.bestBefore}</p>
                                        </td>
                                        <td><img src=${data.imageUrl} style="width: 150px; height:150px; object-fit:cover;"></td>
                                        <td>
                                            <a href="/admin/detail-menu?menuId=${data.id}" class="btn btn-primary">
                                                <i class="bi bi-eye-fill"></i>
                                            </a>
                                            <a href="/admin/edit-menu?menuId=${data.id}" class="btn btn-warning" id="edit-menu">
                                                <i class="bi bi-pencil-fill"></i>
                                            </a>
                                            <button class="btn btn-danger delete-menu" data-id=${data.id}>
                                                <i class="bi bi-trash-fill"></i>
                                            </button>
                                        </td>
                                    </tr>`)
        })
        $('#loader').hide();

        $('.delete-menu').click(function(e){
            const id = $(this).data('id');
            if (confirm('delete this menu?')) {
                deleteMenu(id)
            }
        })
    }
}

$(document).ready(function () {
    getMenu();
});