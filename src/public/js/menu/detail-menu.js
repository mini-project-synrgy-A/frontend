const reponseToken = JSON.parse(localStorage.getItem("token"));

const formatMoney = (money) => {
    return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

const getMenu = async (menuId) => {
    const response = await fetch(BASE_PATH + '/menu/detail/' + menuId, { method: 'get' });
    const result = await response.json()
    const menu = result.data
    const price = formatMoney(menu.price)

    let tags = menu.Tags.map((tag) => tag.name).join(", ");
    console.log(menu)
    console.log(menu.Tags)
    $('.content-detail').append(`<div class="row">
                                    <div class="col-8">
                                        <h3>${menu.name}</h3>
                                        <p class="fw-bold">Rp. ${price}</p>
                                        <p class="text-muted">${tags}</p><br>
                                        <p>Deskripsi:</p>
                                        <p>${menu.description}</p><br>
                                        <p>Informasi Kadaluarsa: </p>
                                        <p class="fst-italic">${menu.bestBefore}</p>
                                        <p class="text-muted">Terakhir diperbarui: ${getDatetime(menu.updatedAt)}</p>
                                    </div>
                                    <div class="col-4">
                                        <img src="${menu.imageUrl}" alt="" class="w-100">
                                    </div>
                                </div>`);
}

//post json
$(document).ready(function () {
    $('.content-detail').html()
    const menuId = $('.detail').data('id')
    console.log(menuId)
    getMenu(menuId)
});