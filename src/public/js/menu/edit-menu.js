const reponseToken = JSON.parse(localStorage.getItem("token"));

const getCategory = async () => {
    const resCategory = await fetch(BASE_PATH + '/menu-category/all', { method: 'get' });
    const categories = await resCategory.json()
    console.log(categories.data)
    categories.data.forEach(category => {
        console.log(category.categoryName)
        $('#category').append(`<option value=${category.id}>${category.categoryName}</option>`)
    });
}

//post json
$(document).ready(function () {
    const menuId = $('.detail').data('id')
    getCategory()
    getMenu(menuId)
    const formCreate = $('#edit_menu');
    $('#form_message').hide();
    formCreate.on('submit', function (e) {
        // di klik submit / button create
        const menuId = $('.detail').data('id')
        e.preventDefault();
        const name = $('#menu-name').val();
        const price = $('#menu-price').val();
        const description = $('#description').val();
        const bestBefore = $('#best-before').val();
        const image = $('#image').prop('files')[0]

        const category = $('select[id=category] option').filter(':selected').val()
        
        console.log(category + name + price + description + bestBefore);
        console.log("image: " + image)
        editMenu(menuId, category, name, price, description, bestBefore, image)
    });
});

const getMenu = async (menuId) => {
    const response = await fetch(BASE_PATH + '/menu/detail/' + menuId, { method: 'get' });
    const result = await response.json()
    const menu = result.data
    console.log(menu)
    console.log(menu.Tags)
    $('#category').val(menu.MenuCategoryId)
    $('#menu-name').val(menu.name);
    $('#menu-price').val(menu.price);
    $('#description').val(menu.description);
    $('#best-before').val(menu.bestBefore);
}

async function editMenu(menuId, category, name, price, description, bestBefore, image) {
    var myHeaders = new Headers();
    myHeaders.append("authorization", "Bearer " + reponseToken.token);

    var formdata = new FormData();
    if(image !== undefined) {formdata.append("image", image, image.name)};
    formdata.append("name", name);
    formdata.append("menu_category_id", category);
    formdata.append("price", price);
    formdata.append("description", description);
    formdata.append("best_before", bestBefore);
    formdata.append("status", "ready");

    var requestOptions = {
        method: 'PATCH',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
    };

    console.log(image)
    const response = await fetch(BASE_PATH + '/menu/edit/' + menuId, requestOptions)

    const data = await response.json();
    console.log(data);
    if (data.status === 'error' || data.status === 'forbidden') {
        $('#form_message').show();
        $('#message').html(data.message);
    } else {
        alert(data.status)
        window.location.href = '/admin/list-menu';
    }
}
