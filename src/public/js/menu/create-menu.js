const reponseToken = JSON.parse(localStorage.getItem("token"));

const getCategory = async () => {
    const resCategory = await fetch(BASE_PATH + '/menu-category/all', { method: 'get' });
    const categories = await resCategory.json()
    console.log(categories.data)
    categories.data.forEach(category => {
        console.log(category.categoryName)
        $('#category').append(`<option value=${category.id}>${category.categoryName}</option>`)
    });
}

//post json
$(document).ready(function () {
    getCategory()
    const formCreate = $('#create_menu');
    $('#form_message').hide();
    $('#loader').hide();
    formCreate.on('submit', function (e) {
        // di klik submit / button create
        e.preventDefault();
        console.log('test');
        const name = $('#menu-name').val();
        const price = $('#menu-price').val();
        const description = $('#description').val();
        const bestBefore = $('#best-before').val();
        const image = $('#image').prop('files')[0]
        const tags = $('#tags').val().split(', ');

        const category = $('select[id=category] option').filter(':selected').val()
        
        console.log(category + name + price + description + bestBefore, tags);
        console.log("image: " + image)
        $('#loader').show();
        createMenu(category, name, price, description, bestBefore, image, tags)
        $('#loader').hide();
    });
});

async function createMenu(category, name, price, description, bestBefore, image, tags) {
    var myHeaders = new Headers();
    myHeaders.append("authorization", "Bearer " + reponseToken.token);

    var formdata = new FormData();
    if(image) {
        formdata.append("image", image, image.name);
    }
    formdata.append("name", name);
    formdata.append("menu_category_id", category);
    formdata.append("price", price);
    formdata.append("description", description);
    formdata.append("best_before", bestBefore);
    formdata.append("status", "ready");
    tags.forEach(tag => formdata.append("tags", tag));

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
    };

    const response = await fetch(BASE_PATH + '/menu/create', requestOptions)

    const data = await response.json();
    console.log(data);
    if (data.status === 'error' || data.status === 'forbidden') {
        $('#form_message').show();
        $('#message').html(data.message);
    } else {
        alert('Berhasil menambahkan menu ' + data.data.name)
        window.location.href = '/admin/list-menu';
    }
}
