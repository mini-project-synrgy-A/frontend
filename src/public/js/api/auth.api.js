const token = JSON.parse(localStorage.getItem('token'));

async function authLogin(username, password) {
  try {
    const response = await fetch(BASE_PATH + '/auth/login', {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'post',
      body: JSON.stringify({
        username,
        password,
      }),
    });

    return await response.json();
  } catch (error) {
    console.log(error);
  }
}
