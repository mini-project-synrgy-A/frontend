const token = JSON.parse(localStorage.getItem('token'));
async function getDetailCart() {
  try {
    const cart = await fetch(BASE_PATH + '/carts/detail', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token.token,
      },
      method: 'get',
    });

    const result = await cart.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}

async function createCart(data) {
  try {
    const cart = await fetch(BASE_PATH + '/carts/create', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token.token,
      },
      method: 'post',
      body: JSON.stringify(data),
    });

    const result = await cart.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}

async function deleteAllUserCart() {
  try {
    const cart = await fetch(BASE_PATH + '/carts', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token.token,
      },
      method: 'delete',
    });

    const result = await cart.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}
