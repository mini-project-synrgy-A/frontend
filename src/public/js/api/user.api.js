async function getAllUser(roleId) {
  try {
    var url = new URL(BASE_PATH + '/profiles');
    var params = { roleId };
    url.search = new URLSearchParams(params).toString();

    const res = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'get',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

async function deleteUser(userId) {
  try {
    var url = new URL(BASE_PATH + '/users/' + userId);

    const res = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'delete',
    });
    const result = await res.json();
    return result;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function getUserDetail() {
  try {
    if (tokenData === null) {
      tokenData = JSON.parse(localStorage.getItem('token'));
    }
    console.log(tokenData);
    const response = await fetch(BASE_PATH + '/users/detail', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'get',
    });

    const res = await response.json();
    return res.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function getProfile() {
  try {
    const url = BASE_PATH + '/profiles/detail/';
    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'get',
    });

    return await response.json();
  } catch (error) {
    console.log(error);
  }
}
