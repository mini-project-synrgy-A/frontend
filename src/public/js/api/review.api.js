const token = JSON.parse(localStorage.getItem('token'));
async function getReview(orderId) {
  try {
    const cart = await fetch(BASE_PATH + '/reviews/all?OrderId='+ orderId, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'get',
    });

    const result = await cart.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}

async function createReview(data) {
  try {
    const cart = await fetch(BASE_PATH + '/reviews/create', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token.token,
      },
      method: 'post',
      body: JSON.stringify(data),
    });

    const result = await cart.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}

async function deleteReview(reviewId) {
  try {
    const cart = await fetch(BASE_PATH + '/reviews/delete/' + reviewId, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token.token,
      },
      method: 'delete',
    });

    const result = await cart.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}
