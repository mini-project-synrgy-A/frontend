async function getFavorite(data) {
  try {
    const url = BASE_PATH + '/favorite/all';
    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + reponseToken.token,
      },
      method: 'GET',
    });
    const result = await response.json();
    return result.data;
  } catch (error) {
    console.log(error);
  }
}

async function menuDetail(menuId) {
  try {
    var url = new URL(BASE_PATH + '/menu/detail/' + menuId);
    const res = await fetch(url, {
      method: 'get',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function getReview(menuId) {
  $('.reviews').empty();
  const response = await fetch(BASE_PATH + '/reviews/all?MenuId=' + menuId, {
    method: 'get',
  });
  const respond = await response.json();

  let data = {};

  if (respond.status === 'error' || respond.status === 'forbidden') {
    console.error(respond.message);
  } else if (respond.status === 500){
    console.error(respond.message);
  } else {
    respond.data.forEach((data) => {
      $('.reviews').append(generateReview(data))
    })
  }
}

const generateReview = (data) => {
  const content = document.createElement("div");
  content.classList.add("review-content");

  const div2 = document.createElement("div")
  div2.classList.add("col-2")

  const img = document.createElement("img")
  img.src = "https://i.pinimg.com/474x/f9/61/30/f961306179ce606db7bedb1d731436b6.jpg"
  img.classList.add("review-profile")
  img.classList.add("rounded-circle")
  div2.appendChild(img)
  content.append(div2);

  const div10 = document.createElement("div");
  div10.classList.add("col-10");

  const divReview = document.createElement("div");
  divReview.classList.add("review");
  divReview.classList.add("row");

  const p = document.createElement("p")
  p.innerHTML = data.review
  divReview.appendChild(p)

  const divRate = document.createElement("div")
  divRate.classList.add("review-rating")
  
  // star
  for (let i = 0; i < 5; i++) {
    if (i < data.rate) {
      const span = document.createElement("span")
      const star = document.createElement("i")
      star.classList.add("bi");
      star.classList.add("bi-star-fill");
      span.appendChild(star)
      divRate.appendChild(span);
    } else {
      const span = document.createElement("span")
      const star = document.createElement("i")
      star.classList.add("bi");
      star.classList.add("bi-star");
      span.appendChild(star)
      divRate.appendChild(span);
    }
  }
  divReview.appendChild(divRate)
  div10.appendChild(divReview)
  content.appendChild(div10)
  return content
}

// $(document).ready(function () {
//   const menuId = $('.menuId').val();
//   console.log("menuId:" + menuId);



//   menuDetail(menuId);
//   getReview(menuId);
// });