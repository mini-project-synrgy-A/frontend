async function getAllOrder() {
  try {
    var url = new URL(BASE_PATH + '/order/all');
    const res = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'get',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return [];
  }
}

async function getDetailOrder(orderId) {
  try {
    var url = new URL(BASE_PATH + '/order/detail/' + orderId);
    const res = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'get',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function createOrder(data) {
  try {
    var url = new URL(BASE_PATH + '/order/create');
    const res = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'post',
      body: JSON.stringify(data),
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function setDoneOrder(orderId) {
  try {
    var url = new URL(BASE_PATH + '/order/done/' + orderId);
    const res = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'PATCH',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function paidOrder(orderId) {
  try {
    var url = new URL(BASE_PATH + '/order/paid/' + orderId);
    const res = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'PATCH',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function cancelOrder(orderId) {
  try {
    var url = new URL(BASE_PATH + '/order/cancel/' + orderId);
    const res = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + tokenData.token,
      },
      method: 'PATCH',
    });
    const result = await res.json();
    return result.data;
  } catch (error) {
    console.log(error);
    return {};
  }
}
