const adminRouter = require('./admin.router');

const router = require('express').Router();

const routes = () => {
  router.get('/', (req, res) => {
    res.render('pages/landing', { title: 'Foodie' });
  });

  router.get('/login', (req, res) => {
    res.render('pages/login', { title: 'login' });
  });

  router.get('/register', (req, res) => {
    res.render('pages/register', { title: 'Register' });
  });

  router.get('/menu', (req, res) => {
    res.render('pages/menu', { title: 'Menu Page', categoryId: null });
  });

  router.get('/favorite', (req, res) => {
    res.render('pages/menu', { title: 'Menu Page', categoryId: 'fav' });
  });

  router.get('/cart', (req, res) => {
    res.render('pages/konfirmasi-pesanan', { title: 'Cart' });
  });

  router.get('/menu/category/:categoryId', (req, res) => {
    res.render('pages/menu', { title: 'Menu Page', categoryId: req.params.categoryId });
  });

  router.get('/deskripsi', (req, res) => {
    res.render('pages/deskripsi', { title: 'Deskripsi Makanan', menuId: req.query.menuId });
  });

  router.get('/about-us', (req, res) => {
    res.render('pages/about-us', { title: 'About Us' });
  });

  router.get('/home', (req, res) => {
    res.render('pages/landing', { title: 'Foodie' });
  });

  router.get('/profile', (req, res) => {
    res.render('pages/profile', { title: 'Profil' });
  });

  router.put('/profile', (req, res) => {
    res.render('pages/profile', { title: 'Profil' });
  });

  router.get('/list-menu', (req, res) => {
    res.render('pages/admin/menu/list-menu', { title: 'Daftar Menu' });
  });

  router.get('/create-menu', (req, res) => {
    res.render('pages/admin/menu/create-menu', { title: 'Buat Menu' });
  });

  router.get('/detail-menu', (req, res) => {
    res.render('pages/admin/menu/detail-menu', { title: 'Detail Menu', menuId: req.query.menuId });
  });

  router.get('/edit-menu', (req, res) => {
    res.render('pages/admin/menu/edit-menu', { title: 'Perbarui Menu', menuId: req.query.menuId });
  });

  router.get('/notes', (req, res) => {
    res.render('pages/notes', { title: 'Tambah Catatan' });
  });

  router.get('/order-history', (req, res) => {
    res.render('pages/order/order-history', { title: 'Riwayat Pesanan' });
  });

  router.get('/order-detail', (req, res) => {
    res.render('pages/order/order-detail', { title: 'Rincian Pesanan', orderId: req.query.orderId });
  });

  router.use('/admin', adminRouter());

  return router;
};

module.exports = routes;
