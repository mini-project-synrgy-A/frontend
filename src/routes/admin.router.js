const router = require('express').Router();
const adminRouter = () => {
  router.get('/list-menu', (req, res) => {
    res.render('pages/admin/menu/list-menu', { title: 'Daftar Menu' });
  });

  router.get('/create-menu', (req, res) => {
    res.render('pages/admin/menu/create-menu', { title: 'Buat Menu' });
  });

  router.get('/detail-menu', (req, res) => {
    res.render('pages/admin/menu/detail-menu', { title: 'Detail Menu', menuId: req.query.menuId });
  });

  router.get('/edit-menu', (req, res) => {
    res.render('pages/admin/menu/edit-menu', { title: 'Perbarui Menu', menuId: req.query.menuId });
  });

  router.get('/customer/customer-edit', (req, res) => {
    res.render('pages/admin/customer/customer-list', { title: 'Daftar Pelanggan' });
  });

  router.get('/user', (req, res) => {
    res.render('pages/admin/user/user-list', { title: 'Daftar Pelanggan' });
  });

  router.get('/order', (req, res) => {
    res.render('pages/admin/order/order-list', { title: 'Daftar Pesanan' });
  });

  return router;
};

module.exports = adminRouter;
