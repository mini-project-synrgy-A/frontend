const express = require('express');
const routes = require('./routes');

const app = express();
const PORT = process.env.PORT || 3030;

// Static Files
app.set('views', './src/views');
app.set('view engine', 'ejs');
app.use('/static', express.static('./src/public'));
app.use('/', routes());

app.use(function (err, req, res, next) {
  if (err) {
    console.error(err.stack);
  }

  res.status(500).send('Something broke!');
});

// Listen on Port 3000
app.listen(PORT, () => console.info(`App listening on port ${PORT}`));
